package com.example.toship.ChitChat;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChatActivity extends AppCompatActivity {

    @BindView(R.id.chatETT)
    EditText mEditChat;
    @BindView(R.id.chatListRecyclerView)
    RecyclerView mChatList;

    private DatabaseReference mDatabaseReference;
    ArrayList<ChatMessage> chatArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        ButterKnife.bind(this);

        mChatList.setLayoutManager(new LinearLayoutManager(this));
        final ChatAdapter chatAdapter = new ChatAdapter(chatArrayList);
        mChatList.setAdapter(chatAdapter);

        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mDatabaseReference.child("message").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                System.out.println(dataSnapshot.getValue());
                ChatMessage chat = dataSnapshot.getValue(ChatMessage.class);
                if(chat != null)
                    chatArrayList.add(chat);
                mChatList.setAdapter(chatAdapter);
                //Toast.makeText(getApplicationContext(),chat.mText,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    @OnClick(R.id.fab)
    public void uploadText(){
        String message = mEditChat.getText().toString();
        Date date = Calendar.getInstance().getTime();
        ChatMessage modalChat = new ChatMessage("Toship", "Sonkusare", message,
                Util.getDate(date));
        mDatabaseReference.child("message").push().setValue(modalChat);
        mEditChat.setText("");
    }



}
