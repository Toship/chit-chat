package com.example.toship.ChitChat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Toship on 27-04-2018.
 * My utility class
 */

public class Util {

    static String getDate(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy MM dd hh mm ss", Locale.US);
        return format.format(date);
    }

    public static Date getDate(String date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy MM dd hh mm ss", Locale.US);
        try {
            return format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

}
