package com.example.toship.ChitChat;

/**
 * Created by Toship on 27-04-2018.
 * User data model
 */

public class UserData {

    private String name, profilePicUrl, email;

    public UserData(){}

    public UserData(String name, String profilePicUrl, String email) {
        this.name = name;
        this.profilePicUrl = profilePicUrl;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
