package com.example.toship.ChitChat;

/**
 * Created by Toship on 27-04-2018.
 * ChatModel
 */

public class ChatMessage {

    private String sender, receiver, text, time;

    ChatMessage(String sender, String receiver, String text, String time) {
        this.sender = sender;
        this.receiver = receiver;
        this.text = text;
        this.time = time;
    }

    public ChatMessage() {

    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSender() {
        return sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public String getText() {
        return text;
    }

    public String getTime() {
        return time;
    }



}
