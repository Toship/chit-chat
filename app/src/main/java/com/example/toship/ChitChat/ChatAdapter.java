package com.example.toship.ChitChat;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Toship on 27-04-2018.
 * Adapter for chat
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyHolder> {

    private ArrayList<ChatMessage> chatArrayList;

    ChatAdapter(ArrayList<ChatMessage> chatArrayList) {
        this.chatArrayList = chatArrayList;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_chat, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {

        holder.message.setText(chatArrayList.get(position).getText());
        holder.time.setText(chatArrayList.get(position).getTime());
    }

    @Override
    public int getItemCount() {
        try {
            return chatArrayList.size();
        } catch (NullPointerException e) {
            return 0;
        }
    }

    class MyHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.messageTV)
        TextView message;
        @BindView(R.id.timeTV)
        TextView time;

        MyHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }

}
