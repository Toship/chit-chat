package com.example.toship.ChitChat;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Toship on 26-04-2018.
 * Adapter for user list
 */

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<UserData> userDataArrayList;

    UserAdapter(Context context, ArrayList<UserData> usersArrayList) {
        mContext = context;
        userDataArrayList = usersArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_user, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserAdapter.MyViewHolder holder, int position) {
        holder.mUserName.setText(userDataArrayList.get(position).getName());
        Uri uri = Uri.parse(userDataArrayList.get(position).getProfilePicUrl());
        Picasso.with(mContext).load(uri).error(R.drawable.profile_pic).into(holder.mProfilePic);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ChatActivity.class);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return userDataArrayList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.profilePicIV)
        ImageView mProfilePic;
        @BindView(R.id.userNameTV)
        TextView mUserName;
        @BindView(R.id.descriptionTV)
        TextView mUserDescription;

        MyViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }


    }

}
