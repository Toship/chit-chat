package com.example.toship.ChitChat;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Toship on 27-04-2018.
 * Preference class
 */

public class Preference {

    public static void setName(Context context, String name){
        SharedPreferences.Editor sharedPreferences =
                context.getSharedPreferences("userData", Context.MODE_PRIVATE).edit();
        sharedPreferences.putString("Name", name);
        sharedPreferences.apply();
    }

    public static void setEmail(Context context, String email){
        SharedPreferences.Editor sharedPreferences =
                context.getSharedPreferences("userData", Context.MODE_PRIVATE).edit();
        sharedPreferences.putString("Email", email);
        sharedPreferences.apply();
    }

    public static void setUrl(Context context, String url){
        SharedPreferences.Editor sharedPreferences =
                context.getSharedPreferences("userData", Context.MODE_PRIVATE).edit();
        sharedPreferences.putString("Url", url);
        sharedPreferences.apply();
    }

    public static String getName(Context context){
        SharedPreferences preferences =
                context.getSharedPreferences("userData", Context.MODE_PRIVATE);
        return preferences.getString("Name", null);
    }

    public static String getEmail(Context context){
        SharedPreferences preferences =
                context.getSharedPreferences("userData", Context.MODE_PRIVATE);
        return preferences.getString("Email", null);
    }

    public static String getUrl(Context context){
        SharedPreferences preferences =
                context.getSharedPreferences("userData", Context.MODE_PRIVATE);
        return preferences.getString("Url", null);
    }

}
